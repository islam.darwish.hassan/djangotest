from django.contrib import admin

from .models import Book, Choice, Question


class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3

class BookInline(admin.StackedInline):
    model = Book

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date']}),
    ]
    inlines = [ChoiceInline]
    list_filter = ['pub_date']
    search_fields = ['question_text']
    list_display = ('question_text', 'pub_date', 'was_published_recently')

admin.site.register(Question, QuestionAdmin)

class BookAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['book_name']}),
        ('Date information', {'fields': ['question']}),
    ]
    search_fields = ['book_name']
    list_display = ('book_name', 'question')
admin.site.register(Book, BookAdmin)
