from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone

from .models import Product


from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
# from PIL import Image,ImageDraw
import requests

size = 128, 128

# im = Image.open(requests.get("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Google_Ads_logo.svg/1200px-Google_Ads_logo.svg.png", stream=True).raw)
# Create your views here.
from PIL import Image, ImageFilter
import base64
import io
import pyautogui



class IndexView(generic.ListView):
    template_name = 'products/index.html'
    context_object_name = 'latest_products_list'

    def get_queryset(self):
        # im = Image.open(requests.get("https://scontent.fcai3-1.fna.fbcdn.net/v/t1.0-9/151959460_4090969704270051_2168992746604719863_n.jpg?_nc_cat=105&ccb=3&_nc_sid=e3f864&_nc_ohc=4IYmjXW4zEgAX_OTe_i&_nc_ht=scontent.fcai3-1.fna&oh=1e4d7417d1e4fb4ac0643fe82f07b4bb&oe=60577518", stream=True).raw)
        im=pyautogui.screenshot()
        # im = im.filter(ImageFilter.BLUR)
        im =im.convert("L")
        buffer = io.BytesIO()
        im.save(buffer, format='PNG')
        buffer.seek(0)
        data_uri = base64.b64encode(buffer.read()).decode('ascii')

        """Return the last ten published products."""
        return data_uri
