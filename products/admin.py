from products.models import Product
from django.contrib import admin

# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['product_name']}),
        ('Date information', {'fields': ['product_desc','pub_date','product_price']}),
    ]
    search_fields = ['product_name']
    list_filter = ['pub_date']
    list_display = ('product_name', 'pub_date')
admin.site.register(Product, ProductAdmin)
