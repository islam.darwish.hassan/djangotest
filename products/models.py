from django.db import models

# Create your models here.

class Product(models.Model):
    product_name = models.CharField(max_length=200)
    product_desc = models.CharField(max_length=300)
    product_price = models.IntegerField(default=0)
    pub_date = models.DateTimeField('date published')

